#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main()
{
	cout << "Output file name:" << endl;
	string szFileName;
	cin >> szFileName;
	cout << "Output file name to open: " << szFileName << endl;

	ofstream file;
	file.open(szFileName);
	if (file.is_open()) {
		file << "Let this be the file contents!";
		file.close();
	}
	else
		cout << "Could not open file: " << szFileName << endl;
	return 0;
}
