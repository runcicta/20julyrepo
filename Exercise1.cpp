#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>


int main()
{
	//define table dimension s
	int r = 0;
	int c = 0;
	std::cout << "Insert the number of rows" << std::endl;
	std::cin >> r;
	std::cout << "Insert the number of columns" << std::endl;
	std::cin >> c;

	//matrix memory allocation
	float **arrf = (float **)malloc(r * sizeof(float *));
	for (int i = 0; i < r; i++)
	{
		arrf[i] = (float *)malloc(c * sizeof(float));
	}
	
	//input numbers
	std::cout << "Insert numbers..." << std::endl;
	for (int i = 0; i < r; i++)
	{
		for (int j = 0; j < c; j++)
		{

				std::cin >> arrf[i][j];

		}
	}

	//printing table header
	int columnwidth = 12;
	for (int i = 0; i < c; i++)
	{
		if (i>0)
		{
			columnwidth = 10;
		}
		std::cout << std::setw(columnwidth) << "[" << i << "]";
	}
	std::cout << std::endl;

	
	//priting matrix
	columnwidth = 12;
	for (int i = 0; i < r; i++)
	{
		std::cout << "[" << i << "]";
		for (int j = 0; j < c; j++)
		{
			std::cout << std::setw(columnwidth) <<std::fixed <<std::setprecision(2) << arrf[i][j];	
		}
		std::cout << std::endl;

		
	}


}
