#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main()
{
	cout << "Input file name:" << endl;
	string szFileName;
	cin >> szFileName;
	cout << "File name to open: " << szFileName << endl;

	ifstream file;

	file.open(szFileName);
	char szTextLine[64];

	while (file.getline(szTextLine, 64)) {
		cout << ">" << szTextLine << endl;
	}

	file.close();

	return 0;
}
